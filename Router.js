import React, {Component} from 'react'
import { Route, NativeRouter, Switch,Redirect } from 'react-router-native'
import LoginPage from './Containers/LoginPage'
import ProfilePage from './Containers/ProfilePage'
import ListPage from './Containers/ListPage'
import ProductPage from './Containers/ProductPage'
import EditProfile from './Containers/EditProfile'
import EditProduct from './Containers/EditProduct'
import AddProduct from './Containers/AddProduct'
import listProduct from './Comonents/listProduct'
import itemProduct from './Comonents/itemProduct'
import {Provider} from 'react-redux'
import store from './store';


class Router extends Component {
    render() {
        return (
            <Provider store={store}>
            <NativeRouter>
                <Switch>
                    <Route exact path='/LoginPage' component={LoginPage}/>
                    <Route exact path='/ProfilePage' component={ProfilePage}/>
                    <Route exact path='/ListPage' component={ListPage}/>
                    <Route exact path='/ProductPage' component={ProductPage}/>
                    <Route exact path='/EditProfile' component={EditProfile}/>
                    <Route exact path='/EditProduct' component={EditProduct}/>
                    <Route exact path='/AddProduct' component={AddProduct}/>
                    <Route exact path='/itemProduct' component={itemProduct}/>
                    <Route exact path='/listProduct' component={listProduct}/>
                    <Redirect to="/LoginPage" />
                </Switch>
            </NativeRouter>
            </Provider>
        )
    }
}
export default Router