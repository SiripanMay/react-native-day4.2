import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Alert } from 'react-native';
import { Button, Icon } from '@ant-design/react-native';
import { connect } from 'react-redux'

class LoginPage extends Component {


  state = {
    username: '',
    password: ''
  }


  onClickLogin = () => {
    const {addTodo,addProduct}=this.props
    this.props.history.push('/ListPage',addTodo(this.state.username),addProduct())
    
  }
  // onClickLogin = () => {
  //   const {addProduct}=this.props
  //   this.props.history.push('/listProduct',addProduct())
    
  // }


  render() {

    return (
      <View style={styles.container} >

        <View style={styles.content}>
          <View style={[styles.box1, styles.center]}>
            <Image source={require('./cat.jpg')} style={[styles.img, styles.center]} />
          </View>

          <View style={[styles.box2, , styles.center]}>
            <View style={[styles.inputBox1, styles.center]}>
              <View style={styles.textInputBox}>
                <TextInput
                  style={styles.textInput}
                  placeholder='username'
                  onChangeText={value => { this.setState({ username: value }) }} />
              </View>
              <View style={styles.textInputBox}>
                <TextInput
                  style={styles.textInput}
                  placeholder='password'ot
                  onChangeText={value => { this.setState({ password: value }) }} />
              </View>
            </View>
            <View style={[styles.inputBox1, styles.center]}>
              <View>
                <Button onPress={()=>{this.onClickLogin()}} 
                >Login</Button>

                

              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }
}
const mapStateToProps = (state)=>{
  return {
      account:state.account,
      products:state.products
  }
}
const mapDispatchToProps =(dispatch)=>{
  return {
      addTodo:(username)=>{
          dispatch({
              type:'ADD_ACCOUNT',
              username:username,
              firstname:'',
              lastname:''
          })
      },

      addProduct:()=>{
        dispatch({
            type:'ADD_PRODUCT',
            image:'',
            name:''
        })
      }
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FADBD8',
  },
  content: {
    backgroundColor: 'blue',
    flex: 1,
    flexDirection: 'column'
  },
  box1: {
    backgroundColor: '#FADBD8',
    flex: 1,

  },
  box2: {
    backgroundColor: '#FADBD8',
    flex: 1,

  },
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  img: {
    backgroundColor: 'white',
    borderRadius: 150,
    width: 250,
    height: 250,
  },
  textInput: {
    fontSize: 20,
    fontWeight: 'bold',
    paddingLeft: 100,
    paddingRight: 100,
    backgroundColor: 'white',
  },
  textInputBox: {
    margin: 10,
    flexDirection: 'column',
  },
  inputBox1: {
    flex: 1
  },
  buttonBox: {
    color: 'pink',
  }

});

