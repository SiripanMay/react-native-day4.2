import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Alert } from 'react-native';
import { Button, Icon,Flex } from '@ant-design/react-native';
import { connect } from 'react-redux'

class AddProduct extends Component {

    state = {
        image: '',
        name: ''
      }
    
    goToListPage = () => {
        this.props.history.push('/ListPage')
    }

    onClickSave = () => {
        const {addProduct}=this.props
        this.props.history.push('/ListPage',addProduct(this.state.image,this.state.name))
    }
    render() {
        
        //addProduct(this.state.image,this.state.name)
        return (
            <View style={[styles.container]}>
            <View style={[styles.header, styles.center]}>
                    <TouchableOpacity onPress={this.goToListPage}>
                        <View style={[styles.boxIcon, styles.center]}>
                            <Text style={styles.textHeader}> <Icon name="left" size="md" color="black" /> </Text>
                        </View>
                    </TouchableOpacity>
                    <View style={[styles.box, styles.center]}>
                        <Text style={styles.textHeader}>Add Product</Text>
                    </View>
                </View>

                <View style={[styles.content]}>
                    <Text style={styles.textHead}> img </Text>
                    <TextInput
                        style={styles.textInput}
                        placeholder='URL'
                        onChangeText={value => { this.setState({ image: value }) }}
                     />
                    <Text style={styles.textHead}> name </Text>
                    <TextInput
                        style={styles.textInput}
                        placeholder='name'
                        onChangeText={value => { this.setState({ name: value }) }}
                     />
                
                </View>

                <View style={[styles.footer]}>
                <Flex.Item style={{ paddingLeft: 4, paddingRight: 4, margin: 10 }}>
                    <Button onPress={this.onClickSave}>Save Product</Button>
                </Flex.Item>
            </View>

            </View>


        )
    }
}
const mapStateToProps = (state)=>{
    return {
        products:state.products,
    }
  }
  const mapDispatchToProps =(dispatch)=>{
    return {
        addProduct:(image,name)=>{
            dispatch({
                type:'ADD_PRODUCT',
                image:image,
                name:name
            })
        }
    }
  }
  
  
  export default connect(mapStateToProps, mapDispatchToProps)(AddProduct)

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F08080',
        flex: 1
    },
    textHeader: {
        fontSize: 40,
        textAlign: 'center',
        margin: 10,
    },
    textHead: {
        fontSize: 30,
        color: 'white',
        margin: 10,
    },
    text: {
        fontSize: 20,
        color: 'white',
        left: 30,
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        backgroundColor: 'white',
        flexDirection: 'row',
    },
    content: {
        flex: 1,
    },
    footer: {
        flexDirection: 'row',
    },
    box: {
        backgroundColor: '#FADBD8',
        flex: 1,
        
        alignItems: 'center',
        justifyContent: 'center'
    },
    boxIcon: {
        backgroundColor: '#FADBD8',
        flex: 0,
       
    },
    textInput: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingLeft: 100,
        paddingRight: 100,
        backgroundColor: 'white',
        margin:5,
    },
});

