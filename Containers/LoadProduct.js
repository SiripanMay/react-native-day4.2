import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Text, View,StyleSheet } from 'react-native';
import { Card, WhiteSpace, WingBlank } from '@ant-design/react-native';
class itemProduct extends Component {

    
    render() {
        const {products}=this.props
        return (

            <Card>
                <Card.Header
                    title={products[products.length-1].name}
                    thumbStyle={{ width: 30, height: 30 }}
                />
                <Card.Body>
                    <View style={{ height: 42 }}>
                        <Text style={{ marginLeft: 16 }}>Card Content</Text>
                    </View>
                </Card.Body>
            </Card>

        )
    }
}
const mapStateToProps = (state)=>{
    return {
        products:state.products,
    }
  }

 export default connect(mapStateToProps)(itemProduct)
  const styles = StyleSheet.create({
  content: {
    flex: 1,
    flexDirection: 'column',
    margin:5
},
row: {
    flex: 1,
    flexDirection: 'row'
  },
  box1: {
    backgroundColor: 'green',
    flex: 1,
    margin: 14,
  },
  box2: {
    backgroundColor: 'purple',
    flex: 1,
    margin: 14,
  },
});