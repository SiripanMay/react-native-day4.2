export default (state = [], action) => {
    switch (action.type) {
        case 'ADD_PRODUCT':
            return [...state,
            {
                image: action.image,
                name: action.name
            }
            ]
        case 'EDIT_PRODUCT':
            return state.filter((item, index) => { index !== action.index })
        default:
            return state

    }
}
