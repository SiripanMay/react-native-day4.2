import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Platform, StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Alert } from 'react-native';
import { Button, Icon, Flex } from '@ant-design/react-native';

class EditProfile extends Component {
    constructor(props) {
        super(props);
        const { account } = this.props
        this.state = {
            username: account[account.length - 1].username,
            firstname: account[account.length - 1].firstname,
            lastname: account[account.length - 1].lastname
        };
    }

    goToProfilePage = () => {
        const { addTodo } = this.props
        this.props.history.push('/ProfilePage', addTodo(this.state.username, this.state.firstname, this.state.lastname))
    }
    goToListPage = () => {
        this.props.history.push('/ListPage')
    }
    render() {
        const { account } = this.props
        return (
            <View style={[styles.container]}>
                <View style={[styles.header, styles.center]}>
                    <TouchableOpacity onPress={this.goToListPage}>
                        <View style={[styles.boxIcon, styles.center]}>
                            <Text style={styles.textHeader}> <Icon name="left" size="md" color="black" /> </Text>
                        </View>
                    </TouchableOpacity>
                    <View style={[styles.box, styles.center]}>
                        <Text style={styles.textHeader}>Edit Profile</Text>
                    </View>
                </View>

                <View style={[styles.content]}>
                    <Text style={styles.textHead}> Username </Text>
                    <TextInput
                        style={styles.textInput}
                        placeholder={account[account.length - 1].username}
                        onChangeText={value => { this.setState({ username: value }) }}
                        value={this.state.username}

                    />
                    <Text style={styles.textHead}> First name </Text>
                    <TextInput
                        style={styles.textInput}
                        placeholder={account[account.length - 1].firstname}
                        onChangeText={value => { this.setState({ firstname: value }) }}
                        value={this.state.firstname}
                    />

                    <Text style={styles.textHead}> Last name</Text>
                    <TextInput
                        style={styles.textInput}
                        placeholder={account[account.length - 1].lastname}
                        onChangeText={value => { this.setState({ lastname: value }) }}
                        value={this.state.lastname}
                    />
                </View>



                <View style={[styles.footer]}>
                    <Flex.Item style={{ paddingLeft: 4, paddingRight: 4, margin: 10 }}>
                        <Button onPress={this.goToProfilePage}>Save Profile</Button>
                    </Flex.Item>
                </View>

            </View >


        )
    }
}
const mapStateToProps = (state) => {
    return {
        account: state.account,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        addTodo: (username, firstname, lastname) => {
            dispatch({
                type: 'ADD_ACCOUNT',
                username: username,
                firstname: firstname,
                lastname: lastname
            })
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(EditProfile)

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F08080',
        flex: 1
    },
    textHeader: {
        fontSize: 40,
        textAlign: 'center',
        margin: 10,
    },
    textHead: {
        fontSize: 30,
        color: 'white',
        margin: 10,
    },
    text: {
        fontSize: 20,
        color: 'white',
        left: 30,
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        backgroundColor: 'white',
        flexDirection: 'row',
    },
    content: {
        flex: 1,
    },
    footer: {
        flexDirection: 'row',
    },
    boxIcon: {
        backgroundColor: '#FADBD8',
        flex: 0,

    },
    box: {
        backgroundColor: '#FADBD8',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textInput: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingLeft: 100,
        paddingRight: 100,
        backgroundColor: 'white',
        margin: 5,
    },
});

