import { createStore, combineReducers } from 'redux';
import AccountReducer from './Reducer/account';
import ProductReducer from './Reducer/product'

const reducers = combineReducers({
    account: AccountReducer,
    products: ProductReducer
})

const store = createStore(reducers);
export default store;
