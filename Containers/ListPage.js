import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Alert, ScrollView } from 'react-native';
import { Button, Icon, Flex, Card,List } from '@ant-design/react-native';
import { connect } from 'react-redux'
import product from '../Reducer/product';
import itemProduct from '../Comonents/itemProduct'
class ListPage extends Component {
    state = {
        items: [],
        image: [],
        name: []
    }

    goToLoginPage = () => {
        this.props.history.push('/listProduct')
    }
    goToProfilePage = () => {
        this.props.history.push('/ProfilePage')
    }
    goToAddProduct = () => {
        this.props.history.push('/AddProduct')
    }
    goToProductPage = () => {
        this.props.history.push('/ProductPage')
    }
    loadData() {
        const { products } = this.props
        let items = []
        for (let i = 0; i < products.length; i++) {
            items = [this.setState({ name: products[i].name })]

        }
        return items
    }
    render() {
        const { products } = this.props

        console.log(products);

        return (
            <View style={[styles.container]}>
                <View style={[styles.header]}>
                    <TouchableOpacity onPress={this.goToLoginPage}>
                        <View style={[styles.boxIcon, styles.center]}>
                            <Text style={styles.textHeader}> <Icon name="left" size="md" color="black" /> </Text>
                        </View>
                    </TouchableOpacity>
                    <View style={[styles.box, styles.center]}>
                        <Text style={styles.textHeader}> ListPage </Text>
                    </View>
                </View>
                <ScrollView >
                    <View style={[styles.content]}>

                    
                    </View>

                </ScrollView>
                <View style={[styles.footer]}>
                    <Flex.Item style={{ paddingLeft: 4, paddingRight: 4, margin: 10 }}>
                        <Button onPress={this.goToLoginPage}>Login</Button>
                    </Flex.Item>
                    <Flex.Item style={{ paddingLeft: 4, paddingRight: 4, margin: 10 }}>
                        <Button onPress={this.goToProfilePage}>Profile</Button>
                    </Flex.Item>
                    <Flex.Item style={{ paddingLeft: 4, paddingRight: 4, margin: 10 }}>
                        <Button onPress={this.goToAddProduct}>Add</Button>
                    </Flex.Item>
                </View>

            </View>


        )
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.products,
    }
}
export default connect(mapStateToProps)(ListPage)

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F08080',
        flex: 1
    },
    textHeader: {
        fontSize: 40,
        textAlign: 'center',
        margin: 10,
    },
    textHead: {
        fontSize: 30,
        color: 'white',
        margin: 10,
    },
    text: {
        fontSize: 20,
        color: 'white',
        left: 30,
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        backgroundColor: 'white',
        flexDirection: 'row',
    },
    content: {
        flex: 1,
        flexDirection: 'column',
        margin: 5
    },
    row: {
        flex: 1,
        flexDirection: 'row'
    },
    box1: {
        backgroundColor: 'green',
        flex: 1,
        margin: 14,
    },
    box2: {
        backgroundColor: 'purple',
        flex: 1,
        margin: 14,
    },

    footer: {
        backgroundColor: 'white',
        flexDirection: 'row',
        backgroundColor: '#FADBD8',
    },
    box: {
        backgroundColor: '#FADBD8',
        flex: 1,

        alignItems: 'center',
        justifyContent: 'center'
    },
    boxIcon: {
        backgroundColor: '#FADBD8',
        flex: 0,

    },
});

